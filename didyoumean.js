const dym = require("didyoumean2")

module.exports = {
  didyoumean: {
    alt: ["dym"],
    fn: function(query, ...matchList) {
      query.value(
        dym(query.value(), matchList, {
          returnType: "first-closest-match",
          trimSpace: true
        })
      )
      return query
    }
  }
}
